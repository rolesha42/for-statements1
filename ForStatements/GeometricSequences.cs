﻿namespace ForStatements
{
    public static class GeometricSequences
    {
        public static ulong GetGeometricSequenceTermsProduct(uint a, uint r, uint n)
        {
            // TODO Task 11. Implement the method that returns the product of geometric sequence terms.
            throw new NotImplementedException();
        }

        public static ulong SumGeometricSequenceTerms(uint n)
        {
            // TODO Task 12. Implement the method that returns sum of a geometric sequence terms when the first term is 5 and the common ratio is 3.
            throw new NotImplementedException();
        }

        public static ulong CountGeometricSequenceTerms1(uint a, uint r, uint maxTerm)
        {
            // TODO Task 13. Implement the method that counts terms in a geometric sequence that are less than or equal to the maxTerm.
            throw new NotImplementedException();
        }

        public static ulong CountGeometricSequenceTerms2(uint a, uint r, uint n, uint minTerm)
        {
            // TODO Task 14. Implement the method that counts terms in a geometric sequence that are greater than or equal to a minTerm.
            throw new NotImplementedException();
        }
    }
}
